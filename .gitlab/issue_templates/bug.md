## Quick Information
- **Python version:** <!-- python3 --version !-->
- **Bot version:** <!-- Command info give you that !-->
- **Client device where the bug occurred:** Computer / Mobile <!-- If needed !-->

## What Happened?
...

## Expected result
...

## Steps to reproduce
...

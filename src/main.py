print("Chargement des extensions & librairie...", end = " ")

import discord
from os import listdir, rename, getcwd
from discord_slash import SlashCommand
from discord.ext import commands
from utils.core import load, addReaction
from utils.page import listReaction
keys = load(["PREFIX", "TOKEN_DISCORD", "DEACTIVATE"])
customPrefix = keys["PREFIX"]

client = commands.Bot(command_prefix = customPrefix, case_insensitive = True, intents = discord.Intents.all())
slash = SlashCommand(client, sync_commands = True)

client.cogs_folder = "cogs"

if keys["DEACTIVATE"] != "None":
    path = getcwd()
    for file in keys["DEACTIVATE"]:
        try:
            rename(f"{path}/{client.cogs_folder}/{file}.py", f"{path}/{client.cogs_folder}/-{file}.py") # désactivation
        except:
            print(f"No file {file} found, check your \"DEACTIVATE\" variable.")
            exit(1)

for file in listdir(client.cogs_folder):
    if file.endswith(".py") and file.startswith("-") == False:
        client.load_extension(f"{client.cogs_folder}.{file[:-3]}")
print("Terminé !")

@client.event
async def on_connect():
    """Triggered when the bot is connected to Discord"""
    print("Connecté !")

@client.event
async def on_disconnect():
    """Triggered when the bot is disconnected to Discord"""
    print("Déconnecté.")

@client.event
async def on_resumed():
    """Triggered when the bot is reconnected to Discord"""
    print("Reconnecté !")

@client.event
async def on_ready(update_changePresence = False):
    """Triggered when the bot is ready to operate"""
    await client.change_presence(status = discord.Status.online, activity = discord.Activity(name = f"{customPrefix}help · {len(client.guilds)} serveur{'s' if len(client.guilds) > 1 else ''}", type = discord.ActivityType.playing))
    if update_changePresence == False:
        print("Bot prêt.")

@client.event
async def on_guild_join(_):
    """Triggered when the bot join a new guild"""
    await on_ready(True)

@client.event
async def on_guild_remove(_):
    """Triggered when the bot left a guild"""
    await on_ready(True)

@client.event
async def on_command_error(ctx, error):
    """Triggered when a command enconter an error"""
    if isinstance(error, commands.errors.CommandNotFound): # si commande inconnu
        if ctx.message: # si pas une commande slash
            if not ctx.invoked_with.startswith(f"{customPrefix}"):
                await addReaction(ctx.message, 1) # ajout réaction
        return
    else:
        await addReaction(ctx.message, 2) # ajout réaction
        raise error

@client.event
async def on_message(message):
    """Triggered a message is received"""
    await client.process_commands(message)

    if message.author == client.user:
        return

    """informations concernant le bot lorsqu'il est mentionner"""
    if client.user.mention == message.content.replace("!",""):
        ctx = await client.get_context(message)
        prefix = await client.get_prefix(message)
        await ctx.send(f">>> Coucou !\nMon préfix est `{prefix}` et ma commande d'aide est `{prefix}help`")

"""Ses évenements sont pour le reminder et le todo"""
@client.event
async def on_raw_reaction_add(payload):
    """Triggered when a reaction is added"""
    message, embed = await listReaction(client, payload)
    if message:
        await message.edit(embed = embed)

@client.event
async def on_raw_reaction_remove(payload):
    """Triggered when a reaction is removed"""
    message, embed = await listReaction(client, payload)
    if message:
        await message.edit(embed = embed)

print("Connexion à Discord...", end = " ")
client.run(keys["TOKEN_DISCORD"])

import discord
from discord.ext import commands, tasks
from discord_slash import cog_ext
from utils.reminder import Reminder, embedListeReminder
from utils.core import getURLsInString, getMentionInString, isSlash, mySendHidden
from utils.core import addReaction, mentionToUser, cleanCodeStringWithMentionAndURLs
from utils.time import stringTempsVersSecondes, nowUTC, intToDatetime, timedeltaToString

def setup(client):
    """Adding Cog to bot"""
    client.add_cog(ReminderDiscord(client))

class ReminderDiscord(commands.Cog, name="Reminder"):
    """Commandes relatives aux reminder."""
    def __init__(self, client):
        self.client = client
        self._reminderLoop.start()
        Reminder().creationTable()

    @tasks.loop(minutes = 1)
    async def _reminderLoop(self):
        """Méthode qui se répète toute les minutes pour vérifier si des rappels n'ont pas expirés, si expirés, les envoient."""
        expiration = Reminder().recuperationExpiration(int(nowUTC())) # on récupères les éléments expirés
        for expired in expiration: # on regarde tout les éléments expirés
            reminder = expired[2] # message
            userID = expired[4] # personne qui a fait le rappel
            channel = self.client.get_channel(expired[0]) # salon du message
            finalEmbed = discord.Embed(description = cleanCodeStringWithMentionAndURLs(reminder), timestamp = intToDatetime(expired[3]), color = discord.Colour.random())
            guild = self.client.get_guild(expired[7])
            if expired[1] == 2 or channel == None or guild.get_member(expired[4]) is None:
                user = self.client.get_user(userID)
                if user == None: # si l'utilisateur n'est pas trouvé
                    return Reminder().suppressionReminder(expired[5]) # suppression du rappel
            if expired[1] == 2: # s'il faut envoyer en DM le message
                channel, userID = await self.reminderCheckDM(userID, expired[5])
            elif channel == None: # si le salon n'existe plus
                channel, userID = await self.reminderCheckDM(userID, expired[5])
                finalEmbed.add_field(name = "Info", value = "Message envoyé en DM car le salon n'est plus disponible.")
            elif guild.get_member(expired[4]) is None: # si l'utilisateur n'est plus dans la guild
                channel, userID = await self.reminderCheckDM(userID, expired[5])
                finalEmbed.add_field(name = "Info", value = f"Message envoyé en DM car vous avez quitté `{guild.name}`.")
            else:
                sourceMessage = expired[6]
                if sourceMessage != None: # vérification message avec slash command et que si c'est pas en DM
                    try:
                        sourceMessage = await channel.fetch_message(sourceMessage) # récupération message
                        await addReaction(sourceMessage, 0) # ajout réaction
                    except:
                        sourceMessage = None # message a été supprimé
            finalEmbed.set_footer(text=f"Message d'il y a {timedeltaToString(int(nowUTC()) - expired[3])}")
            links = ""
            findedURLs = getURLsInString(reminder)
            for i in range(0, len(findedURLs)): # ajout de field "lien" pour pouvoir cliquer sur les liens facilement
                links += f"[Lien {i + 1}]({findedURLs[i]}) · "
            if len(findedURLs) > 0:
                finalEmbed.add_field(name = f"Lien{'s' if len(findedURLs) > 1 else ''}", value = links[:-3])
            message = ""
            if userID != None: # metion de l'utilisateur si le message n'est pas en DM
                message = f"<@{userID}>"
            if expired[1] == 1: # s'il faut mentionner les personnes dans le message
                mentionList = getMentionInString(reminder)
                for i in mentionList:
                    message += f" {i}"
            try:
                await channel.send(message, embed = finalEmbed) # envoie du rappel
            except: # les DM sont fermés
                pass
            Reminder().suppressionReminder(expired[5]) # suppression du rappel
    @_reminderLoop.before_loop
    async def __avant_reminderLoop(self):
        """Wait to start the loop until the whole bot is ready"""
        await self.client.wait_until_ready()

    async def reminderCheckDM(self, userID, reminderID):
        user = self.client.get_user(userID)
        if user == None: # si l'utilisateur n'est pas trouvé
            return Reminder().suppressionReminder(reminderID) # suppression du rappel
        channel = await user.create_dm() # envoie en DM
        return channel, None # plus de mention et plus de message source

    @commands.command(name='reminder', aliases=["remind", "remindme", "rappel"])
    async def _reminder(self, ctx, time, *reminder):
        """Met en place un rappel.⁢⁢⁢⁢⁢\n	➡ Syntaxe: {PREFIX}reminder/remind/remindme/rappel <temps>[@] [message]"""
        _, fromSlash, reminder = isSlash(reminder)
        if len(reminder) > 0:
            reminder = " ".join(reminder)
        else:
            reminder = None

        embed = discord.Embed(color = 0xC41B1B)
        extrarg = 0
        guildID = ctx.guild.id # can be set to 0 if its a DM message, so it can be see from anywhere
        destination = "dans ce salon"
        if not reminder:
            reminder = "Notification"
        if time == "help":
            seconds = 0
        else:
            if time.endswith("@"):
                time = time[:-1]
                extrarg = 1
            if time.lower().endswith("p"):
                time = time[:-1]
                extrarg = 2
                guildID = 0
                destination = "en MP"
            seconds = stringTempsVersSecondes(time)
            if type(seconds) != int:
                if fromSlash != True:
                    await addReaction(ctx.message, 1)
                return await mySendHidden(ctx, fromSlash, seconds)
        if seconds == 0:
            embed.add_field(name="Informations", value=
                "Format pour le temps : `y` ou `a` pour année, `w` pour semaine, `d` ou `j` pour jour, \
                \n`h` pour heure, `m` pour minute, `s` pour seconde (légères variances acceptés aussi). \
                \nNe pas mettre de d'unité reviens a si `2h30` alors c'est `30 minutes`, si c'est juste `30` alors c'est `30 secondes`.\n \
                \nMet un `@` accolée aux temps pour mentionner les gens mentionner dans ton message. \
                \nMet un `P` accolée au temps pour que le bot te DM au lieu de t'envoyer un message dans ce salon."
            )
        elif seconds > (50 * (86400 * 365.242)): # 50 ans
            embed.add_field(name="Attention", value="Tu as spécifié une durée trop longue, la durée maximum étant de 50 ans.")
        else:
            now = int(nowUTC())
            messageID = None
            if fromSlash != True:
                messageID = ctx.message.id
            reminderID = Reminder().ajoutReminder(messageID, ctx.channel.id, extrarg, reminder, now, now + seconds, ctx.author.id, guildID)
            if fromSlash != True:
                await addReaction(ctx.message, 0)
            return await mySendHidden(ctx, fromSlash, f"Reminder **`#{reminderID[0][0]}`** enrengistré ! Notification {destination} dans {timedeltaToString(seconds)}{'' if seconds >= 3600 else ' (avec 1m de retard maximum)'}.")
        await mySendHidden(ctx, fromSlash, embed = embed)
    @_reminder.error
    async def _reminder_error(self, ctx, error):
        """Error command handler"""
        if 'time is a required argument that is missing.' in str(error):
            await ctx.send("Tu n'as pas spécifié de durée.")
    @cog_ext.cog_slash(name="reminder", description = "Met en place un rappel.")
    async def __reminder(self, ctx, time, reminder = None):
        """Slash command"""
        if reminder == None:
            return await self._reminder(ctx, time, True)
        else:
            return await self._reminder(ctx, time, reminder, True)

    @commands.command(name='reminderlist', aliases=["remindlist", "rl", "rappeliste"])
    async def _reminderlist(self, ctx, *arg):
        """Affiche la liste des rappels d'un utilisateur.⁢⁢⁢⁢⁢\n	➡ Syntaxe: {PREFIX}reminderlist/rl/remindlist/rappeliste [utilisateur]"""
        _, fromSlash, arg = isSlash(arg)
        utilisateur = ctx.author
        page = 1
        erreur = False
        if len(arg) > 0:
            for i in range(0, len(arg)):
                try:
                    utilisateur = self.client.get_user(mentionToUser(getMentionInString(arg[i])[0]))
                except:
                    try:
                        page = int(arg[i])
                    except:
                        erreur = True
        if erreur == True:
            return await mySendHidden(ctx, fromSlash, "Veuillez renseigné un utilisateur ou un numéro de page valide.")

        if fromSlash != True:
            await addReaction(ctx.message, 0)

        embed, pageMAX = await embedListeReminder(utilisateur, ctx.guild.id, page)
        message = await ctx.send(embed = embed)
        if pageMAX > 1:
            for emoji in ["⬅️", "➡️"]:
                await message.add_reaction(emoji)
        else:
            await message.add_reaction("🔄")
    @cog_ext.cog_slash(name="reminderlist", description = "Affiche la liste des rappels d'un utilisateur.")
    async def __reminderlist(self, ctx, userorpage = None):
        """Slash command"""
        if userorpage == None:
            return await self._reminderlist(ctx, True)
        else:
            return await self._reminderlist(ctx, userorpage, True)

    @commands.command(name='reminderdelete', aliases=["reminddelete", "rd"])
    async def _reminderdelete(self, ctx, *id):
        """Suppprime un rappel.⁢⁢⁢⁢⁢\n	➡ Syntaxe: {PREFIX}reminderdelete/rd <id>"""
        id, fromSlash, _ = isSlash(id)
        if id:
            try:
                id = int(id)
            except:
                return await mySendHidden(ctx, fromSlash, "L'ID renseigné n'est pas valide.")
        else:
            return await ctx.send("Veuillez renseigner un ID.")

        verification = Reminder().appartenanceReminder(ctx.author.id, id, ctx.guild.id)
        if verification:
            Reminder().suppressionReminder(id)
            if fromSlash != True:
                await addReaction(ctx.message, 0)
            return await mySendHidden(ctx, fromSlash, f"Reminder **#{id}** supprimé !")
        else:
            if fromSlash != True:
                await addReaction(ctx.message, 2)
            return await mySendHidden(ctx, fromSlash, "Rappel non trouvé, pas sur le bon serveur ou qui ne vous appartiens pas.")
    @cog_ext.cog_slash(name="reminderdelete", description = "Suppprime un rappel.")
    async def __reminderdelete(self, ctx, id):
        """Slash command"""
        return await self._reminderdelete(ctx, id, True)

from discord.ext import commands
from discord_slash import cog_ext
from utils.todo import ToDo, embedListeToDo
from utils.core import getMentionInString, isSlash, mySendHidden
from utils.core import addReaction, mentionToUser
from utils.time import nowUTC

def setup(client):
    """Adding Cog to bot"""
    client.add_cog(ToDoDiscord(client))

class ToDoDiscord(commands.Cog, name="Todo"):
    """Commandes relatives aux To Do."""
    def __init__(self, client):
        self.client = client
        ToDo().creationTable()

    @commands.command(name='todo')
    async def _todo(self, ctx, *todo):
        """Met en place un To Do.⁢⁢⁢⁢⁢\n	➡ Syntaxe: {PREFIX}todo <message>"""
        _, fromSlash, todo = isSlash(todo)
        if len(todo) > 0:
            todo = " ".join(todo)
        else:
            todo = None

        if not todo:
            return await mySendHidden(ctx, fromSlash, "Tu n'as pas spécifié de message.")
        else:
            now = int(nowUTC())
            messageID = None
            if len(todo) > 1024:
                return await mySendHidden(ctx, fromSlash, "Message trop long (maximum de 1024 caractères).")
            if fromSlash != True:
                messageID = ctx.message.id
            todoID = ToDo().ajout(messageID, todo, now, ctx.author.id)
            if fromSlash != True:
                await addReaction(ctx.message, 0)
            return await mySendHidden(ctx, fromSlash, f"To Do **`#{todoID[0][0]}`** enrengistré !")
    @cog_ext.cog_slash(name="todo", description = "Met en place un To Do.")
    async def __todo(self, ctx, todo):
        """Slash command"""
        return await self._todo(ctx, todo, True)

    @commands.command(name='todolist', aliases=["tdl"])
    async def _todolist(self, ctx, *arg):
        """Affiche la liste des To Do's d'un utilisateur.⁢⁢⁢⁢⁢\n	➡ Syntaxe: {PREFIX}todolist/tdl [utilisateur]"""
        _, fromSlash, arg = isSlash(arg)
        utilisateur = ctx.author
        page = 1
        erreur = False
        if len(arg) > 0:
            for i in range(0, len(arg)):
                try:
                    utilisateur = self.client.get_user(mentionToUser(getMentionInString(arg[i])[0]))
                except:
                    try:
                        page = int(arg[i])
                    except:
                        erreur = True
        if erreur == True:
            return await mySendHidden(ctx, fromSlash, "Veuillez renseigné un utilisateur ou un numéro de page valide.")

        if fromSlash != True:
            await addReaction(ctx.message, 0)

        embed, pageMAX = await embedListeToDo(utilisateur, page)
        message = await ctx.send(embed = embed)
        if pageMAX > 1:
            for emoji in ["⬅️", "➡️"]:
                await message.add_reaction(emoji)
        else:
            await message.add_reaction("🔄")
    @cog_ext.cog_slash(name="todolist", description = "Affiche la liste des To Do's d'un utilisateur.")
    async def __todolist(self, ctx, userorpage = None):
        """Slash command"""
        if userorpage == None:
            return await self._todolist(ctx, True)
        else:
            return await self._todolist(ctx, userorpage, True)

    @commands.command(name='tododelete', aliases=["tdd"])
    async def _tododelete(self, ctx, *id):
        """Suppprime un To Do.⁢⁢⁢⁢⁢\n	➡ Syntaxe: {PREFIX}tododelete/tdd <id>"""
        id, fromSlash, _ = isSlash(id)
        if id:
            try:
                id = int(id)
            except:
                return await mySendHidden(ctx, fromSlash, "L'ID renseigné n'est pas valide.")
        else:
            return await ctx.send("Veuillez renseigner un ID.")

        verification = ToDo().appartenance(ctx.author.id, id)
        if verification:
            ToDo().suppression(id)
            if fromSlash != True:
                await addReaction(ctx.message, 0)
            return await mySendHidden(ctx, fromSlash, f"To Do **#{id}** supprimé !")
        else:
            if fromSlash != True:
                await addReaction(ctx.message, 2)
            return await mySendHidden(ctx, fromSlash, "To Do non trouvé, ou peut-être qu'il ne vous appartiens pas.")
    @cog_ext.cog_slash(name="tododelete", description = "Suppprime un To Do.")
    async def __tododelete(self, ctx, id):
        """Slash command"""
        return await self._tododelete(ctx, id, True)

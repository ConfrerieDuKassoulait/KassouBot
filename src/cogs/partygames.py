import discord
from discord.ext import commands
import discordTogether
from discord_slash import cog_ext
from discordTogether import DiscordTogether
from utils.core import mySendHidden

def setup(client):
    """Adding Cog to bot"""
    client.add_cog(PartyGames(client))

class PartyGames(commands.Cog, name="Partygames"):
    """Discord beta "party games" dispo uniquement sur PC."""
    def __init__(self, client):
        self.client = client
        self.togetherControl = DiscordTogether(client)
        self.appList = { # appNameForDiscord - (AppNameForConsumer, AppImage)
            "youtube": ("Youtube Together", "https://logo-logos.com/wp-content/uploads/2016/11/YouTube_icon_logo.png"),
            "poker": ("Poker Night", "https://images.launchbox-app.com/21782afd-4f83-492a-b199-38404d743e57.png"),
            "chess": ("Chess in the Park", "https://images.discordapp.net/avatars/716382796108660826/e52b79451f4d00cb04a4aca3099210a7.png?size=512"),
            "betrayal": ("Betrayal.io", "https://1.bp.blogspot.com/-uaqT13tY30Q/X_kqsiBJqTI/AAAAAAAADRw/R5ekQuGsO08dlBlgzXZbHktF3ioHmwmPQCLcBGAsYHQ/w1200-h630-p-k-no-nu/icon%2B%25283%2529.png"),
            "fishing": ("Fishington.io", None)
        }

    @commands.command(name='app', hidden = True)
    async def _linkCreator(self, ctx, app: str = None, fromSlash = None):
        """Renvoie l'embed de l'app selectionée\n	➡ Syntaxe: {PREFIX}app <appName>"""
        if app == None:
            return await mySendHidden(ctx, fromSlash, f"Aucune application renseignée, merci d'en renseigner une (`{ctx.prefix}app <appName>`).")
        appName = app
        appImage = None
        try:
            appName = self.appList[app][0]
            appImage = self.appList[app][1]
        except:
            pass
        try:
            link = await self.togetherControl.create_link(ctx.author.voice.channel.id, app)
        except AttributeError:
            return await mySendHidden(ctx, fromSlash, "Vous devez d'abord être dans un salon vocal avant de faire cette commande.")
        except discordTogether.errors.InvalidActivityChoice:
            return await mySendHidden(ctx, fromSlash, "Cette application n'est pas reconnu par Discord.")
        embed = discord.Embed(title = "Party Games", description = f"[Lancer *{appName}*]({link}) !")
        if appImage:
            embed.set_thumbnail(url = appImage)
        embed.set_footer(text = "Ne fonctionne que sur PC pour le moment.")
        return await ctx.send(embed = embed)


    @commands.command(name='youtube')
    async def _youtube(self, ctx, fromSlash = None):
        """Créer une instance "Youtube Together"."""
        return await self._linkCreator(ctx, "youtube", fromSlash)
    @cog_ext.cog_slash(name="youtube", description = "Créer une instance \"Youtube Together\".")
    async def __youtube(self, ctx):
        """Slash command"""
        return await self._youtube(ctx, True)

    @commands.command(name='poker')
    async def _poker(self, ctx, fromSlash = None):
        """Créer une instance "Poker Night"."""
        return await self._linkCreator(ctx, "poker", fromSlash)
    @cog_ext.cog_slash(name="poker", description = "Créer une instance \"Poker Night\".")
    async def __poker(self, ctx):
        """Slash command"""
        return await self._poker(ctx, True)

    @commands.command(name='chess')
    async def _chess(self, ctx, fromSlash = None):
        """Créer une instance "Chess in the Park"."""
        return await self._linkCreator(ctx, "chess", fromSlash)
    @cog_ext.cog_slash(name="chess", description = "Créer une instance \"Chess in the Park\".")
    async def __chess(self, ctx):
        """Slash command"""
        return await self._chess(ctx, True)

    @commands.command(name='betrayal')
    async def _betrayal(self, ctx, fromSlash = None):
        """Créer une instance "Betrayal.io"."""
        return await self._linkCreator(ctx, "betrayal", fromSlash)
    @cog_ext.cog_slash(name="betrayal", description = "Créer une instance \"Betrayal.io\".")
    async def __betrayal(self, ctx):
        """Slash command"""
        return await self._betrayal(ctx, True)

    @commands.command(name='fishing')
    async def _fishing(self, ctx, fromSlash = None):
        """Créer une instance "Fishington.io"."""
        return await self._linkCreator(ctx, "fishing", fromSlash)
    @cog_ext.cog_slash(name="fishing", description = "Créer une instance \"Fishington.io\".")
    async def __fishing(self, ctx):
        """Slash command"""
        return await self._fishing(ctx, True)

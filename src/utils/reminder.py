from utils.db import Database
from discord import Embed, Colour
from utils.time import nowUTC, intToDatetime, timedeltaToString, timestampScreen

class Reminder(Database):
    def __init__(self):
        super().__init__(r"db/bot.sqlite3")

    def creationTable(self):
        """Créer la table qui stocker les reminders."""
        requete = """
                  CREATE TABLE IF NOT EXISTS reminder (
                      id INTEGER PRIMARY KEY,
                      message_id INTEGER,
                      channel_id INTEGER,
                      extrarg_id INTEGER,
                      reminder_str TEXT,
                      creation_int INTEGER,
                      expiration_int INTEGER,
                      user_id INTEGER,
                      guild_id INTEGER
                  );
                  """
        self.requete(requete)

    def ajoutReminder(self, messageID: int, channelID: int, extrarg: int, reminder: str, creation: int, expiration: int, userID: int, guildID: int):
        """Ajoute un reminder."""
        requete = """
                  INSERT INTO reminder (
                      message_id, channel_id, extrarg_id, reminder_str, creation_int, expiration_int, user_id, guild_id
                  ) VALUES (
                      ?, ?, ?, ?, ?, ?, ?, ?
                  );
                  """
        self.requete(requete, [messageID, channelID, extrarg, reminder, creation, expiration, userID, guildID])
        return self.affichageResultat(self.requete("SELECT last_insert_rowid();"))

    def suppressionReminder(self, id: int):
        """Supprime un reminder."""
        requete = """
                  DELETE FROM reminder
                  WHERE id = ?
                  """
        self.requete(requete, id)

    def listeReminder(self, userID: int, guildID: int):
        """Retourne la liste des reminders d'un utilisateur."""
        requete = """
                  SELECT reminder_str, creation_int, expiration_int, id FROM reminder
                  WHERE user_id = ? AND (guild_id = ? OR guild_id = 0)
                  """
        return self.affichageResultat(self.requete(requete, [userID, guildID]))

    def recuperationExpiration(self, time: int):
        """Récupère les reminders qui sont arrivés à expiration et ses infos."""
        requete = """
                  SELECT channel_id, extrarg_id, reminder_str, creation_int, user_id, id, message_id, guild_id FROM reminder
                  WHERE expiration_int < ?
                  """
        return self.affichageResultat(self.requete(requete, time))

    def appartenanceReminder(self, userID: int, id: int, guildID: int):
        """Vérifie qu'un rappel appartiens à un utilisateur et que la guilde soit la bonne. Renvois False si le rappel n'existe pas."""
        requete = """
                  SELECT EXISTS (
                      SELECT 1 FROM reminder
                      WHERE id = ? AND user_id = ? AND (guild_id = ? OR guild_id = 0)
                  )
                  """
        return True if self.affichageResultat(self.requete(requete, [id, userID, guildID]))[0][0] == 1 else False

async def embedListeReminder(utilisateur, guildID, page, color = None, refresh_message = None):
    """Fais l'embed d'une page pour l'affichage de la liste des reminders d'un utilisateur."""
    reminders = Reminder().listeReminder(utilisateur.id, guildID)
    elementPerPage = 5
    pageMAX = -(-len(reminders) // elementPerPage)

    if refresh_message:
        if len(reminders) > 0:
            if pageMAX > 1 and refresh_message.reactions[0] != "⬅️":
                for emoji in ["⬅️", "➡️"]:
                    await refresh_message.add_reaction(emoji)

    if pageMAX <= 1:
        page = 1 # force page 1
    if color == None:
        color = Colour.random()
    embed = Embed(description = f"**Rappel{'s' if len(reminders) > 1 else ''} de {utilisateur.mention}** • Page {page}/{pageMAX}", color = color)
    embed.set_thumbnail(url = utilisateur.avatar_url_as(size = 64))
    limit = elementPerPage * page
    if (len(reminders) > 0 and page <= pageMAX):
        curseur = limit - (elementPerPage - 1)
        for reminder in reminders[limit - elementPerPage:]:
            if curseur <= limit:
                texte = reminder[0]
                if len(texte) > 1024:
                    texte = f"{texte[:1021]}..."
                expiration = reminder[2] - int(nowUTC())
                if expiration > 0:
                    expiration = f"Expire dans {timedeltaToString(expiration)}"
                else:
                    expiration = f"A déjà expiré."
                embed.add_field(value = texte, name = f"#{reminder[3]} • Fais le {timestampScreen(intToDatetime(reminder[1]))}\n{expiration}", inline = False)
                curseur += 1
    else:
        embed.add_field(name = "\u200b", value = f"L'utilisateur n'a aucun rappel en attente ou page n°{page} vide !")
    embed.set_footer(text = "Les rappels qui ont déjà expirés vont apparaître dans quelques instants.\nIls peuvent avoir jusqu'à 1 minute de retard maximum.")
    return (embed, pageMAX)

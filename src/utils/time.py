from pytz import timezone
from datetime import datetime, timedelta
from re import findall, sub
from utils.core import load

myTimezone = load(["TIMEZONE"])["TIMEZONE"]

def stringTempsVersSecondes(time):
    """Convertis une durée rentrée par un utilisateur en string vers des secondes en int"""
    conversionTemps = {
        "31536000 ": ["y", "a"],
        "604800": ["w"],
        "86400": ["j", "d"],
        "3600": ["h"],
        "60": ["m"],
        "1": ["s"]
    }

    tmp = None
    for item in conversionTemps.items():
        if tmp == True:
            tmp = item[0]
        for unite in item[1]:
            if unite in time:
                tmp = True
    if tmp == None:
        tmp = "1"
    conversionTemps[tmp].append("")
    del tmp

    valeursMultiplicateur = ""
    for i in conversionTemps.values():
        for j in i:
            valeursMultiplicateur += f"{j}|"
    match = findall(f'([0-9]+)({valeursMultiplicateur[:-1]})?', time)

    if not match:
        return "Veuillez entrer un temps valide."

    remindertime = 0
    for i in match:
        for tempsEnSeconde, nomCommun in conversionTemps.items():
            if i[1] in nomCommun:
                remindertime += int(tempsEnSeconde) * int(i[0])

    return remindertime

def nowCustom():
    """Heure de maintenant avec fuseau horaire local en float"""
    return datetime.now(timezone(myTimezone)).timestamp()

def nowUTC():
    """Heure de maintenant en UTC en float"""
    return datetime.utcnow().timestamp()

def UTCDatetimeToCustomDatetime(datetime):
    """Conversion d'une timestamp UTC en timestamp local en datetime"""
    return timezone(myTimezone).fromutc(datetime)

def intToDatetime(intOrFloat):
    """Convertis un int ou float en Datetime"""
    return datetime.fromtimestamp(intOrFloat)

def timestampScreen(timestamp):
    """Affichage d'une timestamp"""
    date_edit = str(UTCDatetimeToCustomDatetime(timestamp)).replace('-', '/').split(' ')
    date = date_edit[0]
    heure = date_edit[1].split('+')[0]
    return f"{date[8:]}/{date[5:-3]}/{date[:4]} à {heure.split('.')[0]}"

def timedeltaToString(time):
    """Différence entre une heure en seconde et maintenant"""
    age = sub(r' days?, ', ':', str(timedelta(seconds = time))).split(':')
    affichage = [1, 1, 1, 1]
    if len(age) == 3:
        affichage = [0, 1, 1, 1]
        age.insert(0, None)
    for i in range(1, len(affichage)):
        if int(age[i]) == 0:
            affichage[i] = 0
    if affichage[0] == 1:
        day = int(age[0]) # récupération du nombre de jour
        year = day // 365 # ajout du nombre d'année
        day -= year * 365 # suppression des années dans le nombre de jour
        week = day // 7 # ajout du nombres de semaines
        day -= week * 7 # suppression des semaines dans le nombre du jour
        year = f"{year} an{'s' if year > 1 else ''}" if year > 0 else ""
        week = f"{week} semaine{'s' if week > 1 else ''}" if week > 0 else ""
        day = f"{day} jour{'s' if day > 1 else ''}" if day > 0 else ""
        _ageTemp = [year, week, day]
        while "" in _ageTemp:
            _ageTemp.remove("")
        age[0] = ', '.join(_ageTemp).strip(' ')
    else:
        age[0] = ""
    age[1] = f"{age[1]}h" if affichage[1] == 1 else ""
    age[2] = f"{age[2]}m" if affichage[2] == 1 else ""
    age[3] = f"{age[3]}s" if affichage[3] == 1 else ""
    while "" in age:
        age.remove("")
    return ', '.join(age).strip(' ')

def getAge(date):
    """Décompose la différence entre une date et maintenant avec les bons timezone"""
    joursRestants = UTCDatetimeToCustomDatetime(intToDatetime(nowUTC())) - UTCDatetimeToCustomDatetime(date)
    years = joursRestants.total_seconds() / (365.242 * 24 * 60 * 60)
    months = (years - int(years)) * 12
    days = (months - int(months)) * (365.242 / 12)
    hours = (days - int(days)) * 24
    minutes = (hours - int(hours)) * 60
    seconds = (minutes - int(minutes)) * 60
    return (int(years), int(months), int(days),  int(hours), int(minutes), int(seconds))

def ageLayout(tuple):
    """Avec la méthode `getAge`, permet de mettre en forme un âge⁢⁢⁢⁢⁢⁢⁢⁢⁢⁢"""
    time = {}
    time[0], time[1], time[2], time[3], time[4], time[5] = "an", "mois", "jour", "heure", "minute", "seconde"
    for i in range(len(tuple)):
        if tuple[i] > 1 and i != 1:
            time[i] = time[i] + "s"
    message = ""
    if tuple[5] > 0:
        affichage = [5]
    if tuple[4] > 0:
        affichage = [4, 5]
    for i in range(3, -1, -1):
        if tuple[i] > 0:
            affichage = [i, i + 1, i + 2]
    for i in affichage:
        message = message + f", {tuple[i]} {time[i]}"
    return message[2:]

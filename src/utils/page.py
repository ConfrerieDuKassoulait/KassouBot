from re import findall
from utils.reminder import embedListeReminder
from utils.todo import embedListeToDo

async def listReaction(client, payload):
    """
    Gère le changement de page du reminderlist ou du todolist avec les réactions.
    Types: 0 -> reminder | 1 -> todo
    """
    if payload.emoji.name in ["⬅️", "🔄", "➡️"]:
        if payload.event_type == "REACTION_ADD":
            if payload.member.bot == True: # vérifie que c'est pas un bot qui a réagit
                return False, False
        channel = client.get_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        if message.author.id != client.user.id or len(message.embeds) == 0: # vérification message du bot + au moins 1 embed
            return False, False
        embed = message.embeds[0].to_dict()
        if "description" in embed: # si c'est un embed avec une description
            if len(findall(r"\*\*(Rappels?|To Do('s)?) de <@\!?\d+>\*\* • Page \d+\/\d+", embed["description"])) == 1: # si c'est le bon embed
                infoDescription = findall(r"\*\*(Rappels?|To Do('s)?) de <@\!?(\d+)>\*\* • Page (\d+)\/(\d+)", embed["description"])[0]
                if "Rappel" in infoDescription[0]:
                    type = 0
                elif "To Do" in infoDescription[0]:
                    type = 1
                else:
                    raise Exception("Unknown type")
                infoDescription = infoDescription[2:]
                utilisateur = client.get_user(int(infoDescription[0]))
                page = int(infoDescription[1])
                refresh_message = None
                if payload.emoji.name == "⬅️":
                    if page > 1:
                        page -= 1
                    else:
                        if int(infoDescription[2]) > 1:
                            page = int(infoDescription[2])
                        else:
                            return False, False
                if payload.emoji.name == "➡️":
                    if page + 1 <= int(infoDescription[2]):
                        page += 1
                    else:
                        if page > 1:
                            page = 1
                        else:
                            return False, False
                if payload.emoji.name == "🔄":
                    refresh_message = message
                if type == 0:
                    embed, pageMAX = await embedListeReminder(utilisateur, payload.guild_id, page, embed["color"], refresh_message)
                elif type == 1:
                    embed, pageMAX = await embedListeToDo(utilisateur, page, embed["color"], refresh_message)
                if pageMAX > 1:
                    await removeReactions(message, ["🔄"])
                else:
                    await message.add_reaction("🔄")
                    await removeReactions(message, ["⬅️", "➡️"])
                return message, embed
    return False, False

async def removeReactions(message, reactions):
    for reaction in message.reactions:
        if str(reaction) in reactions:
            users = await reaction.users().flatten()
            for user in users:
                await message.remove_reaction(reaction, user)

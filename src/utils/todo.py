from utils.db import Database
from discord import Embed, Colour
from utils.time import nowUTC, intToDatetime, timedeltaToString, timestampScreen

class ToDo(Database):
    def __init__(self):
        super().__init__(r"db/bot.sqlite3")

    def creationTable(self):
        """Créer la table qui stocke les To Do."""
        requete = """
                  CREATE TABLE IF NOT EXISTS todo (
                      id INTEGER PRIMARY KEY,
                      message_id INTEGER,
                      todo_str TEXT,
                      creation_int INTEGER,
                      user_id INTEGER
                  );
                  """
        self.requete(requete)

    def ajout(self, messageID: int, todo: str, creation: int, userID: int):
        """Ajoute un To Do."""
        requete = """
                  INSERT INTO todo (
                      message_id, todo_str, creation_int, user_id
                  ) VALUES (
                      ?, ?, ?, ?
                  );
                  """
        self.requete(requete, [messageID, todo, creation, userID])
        return self.affichageResultat(self.requete("SELECT last_insert_rowid();"))

    def suppression(self, id: int):
        """Supprime un To Do."""
        requete = """
                  DELETE FROM todo
                  WHERE id = ?
                  """
        self.requete(requete, id)

    def liste(self, userID: int):
        """Retourne la liste des To Do d'un utilisateur."""
        requete = """
                  SELECT id, todo_str, creation_int FROM todo
                  WHERE user_id = ?
                  """
        return self.affichageResultat(self.requete(requete, userID))

    def appartenance(self, userID: int, id: int):
        """Vérifie qu'un To Do appartiens à un utilisateur. Renvois False si le To Do n'existe pas."""
        requete = """
                  SELECT EXISTS (
                      SELECT 1 FROM todo
                      WHERE id = ? AND user_id = ?
                  )
                  """
        return True if self.affichageResultat(self.requete(requete, [id, userID]))[0][0] == 1 else False

async def embedListeToDo(utilisateur, page, color = None, refresh_message = None):
    """Fais l'embed d'une page pour l'affichage de la liste des To Do d'un utilisateur."""
    todos = ToDo().liste(utilisateur.id)
    elementPerPage = 6
    pageMAX = -(-len(todos) // elementPerPage)

    if refresh_message:
        if len(todos) > 0:
            if pageMAX > 1 and refresh_message.reactions[0] != "⬅️":
                for emoji in ["⬅️", "➡️"]:
                    await refresh_message.add_reaction(emoji)

    if pageMAX <= 1:
        page = 1 # force page 1
    if color == None:
        color = Colour.random()
    s = "'s"
    embed = Embed(description = f"**To Do{s if len(todos) > 1 else ''} de {utilisateur.mention}** • Page {page}/{pageMAX}", color = color)
    embed.set_thumbnail(url = utilisateur.avatar_url_as(size = 64))
    limit = elementPerPage * page
    if (len(todos) > 0 and page <= pageMAX):
        curseur = limit - (elementPerPage - 1)
        for todo in todos[limit - elementPerPage:]:
            if curseur <= limit:
                embed.add_field(value = todo[1], name = f"#{todo[0]} • Fais le {timestampScreen(intToDatetime(todo[2]))}", inline = False)
                curseur += 1
    else:
        embed.add_field(name = "\u200b", value = f"L'utilisateur n'a aucun To Do ou page n°{page} vide !")
    embed.set_footer(text = "Merci de supprimer vos anciens To Do pour faire de la place pour les autres.")
    return (embed, pageMAX)

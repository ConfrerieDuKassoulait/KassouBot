#!/usr/bin/env bash
cd src/lavalink
if [ -f "Lavalink.jar" ]; then
    echo "Lavalink found."
else {
    echo "Downloading Lavalink ...";
    chmod u+x download.sh;
    ./download.sh
} fi
if pgrep -f "Lavalink.jar" > /dev/null
then
    echo "Lavaling already started."
else {
    echo "Starting Lavalink server ...";
    java -Djdk.tls.client.protocols=TLSv1.3 -Xmx3G -jar Lavalink.jar 1> /dev/null & # showing only stderror
} fi
cd ..
echo "Starting Discord bot ..."
python3 -u ./main.py

# if you want to kill the java process, do this command:
# pkill -9 -f Lavalink.jar

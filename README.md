# Bot Discord

[![Version](https://img.shields.io/badge/version-1.6-green?style=for-the-badge)](https://gitlab.com/ConfrerieDuKassoulait/KassouBot/-/releases)
[![Build](https://img.shields.io/gitlab/pipeline/ConfrerieDuKassoulait/KassouBot/main?style=for-the-badge)](https://gitlab.com/ConfrerieDuKassoulait/KassouBot/container_registry)

## __Features__

- First, there is a strong `help` command who explain every single commands.
- Slash commands support (use [Discord Interactions](https://github.com/goverfl0w/discord-interactions)).
- Party games activities support (use [Discord Together](https://github.com/apurv-r/discord-together)).
- Music support (use [Lavalink](https://github.com/freyacodes/Lavalink) with the [Wavelink](https://github.com/PythonistaGuild/Wavelink) client).
- Reminders and Todos list support.
- Poll support.
- Meme from reddit and NSFW pictures support.
- Basics commands and simple games, don't mind on opening an issue if you wan't your idea to be added to the bot.
- Use [discord.py](https://github.com/Rapptz/discord.py) (rewrite).
- Use [SQLite](https://www.sqlite.org/index.html) for the database.


## __Setting up__

You have to replace `TOKEN_DISCORD`, `PREFIX`, `TOKEN_GENIUS`, `TOKEN_REDDIT_CLIENT_ID`, `TOKEN_REDDIT_CLIENT_SECRET`, `TOKEN_REDDIT_USER_AGENT` and [`TIMEZONE`](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) with your desired values. You must also specify a path to the folder where the database will be.

With a [docker-compose](https://gitlab.com/ConfrerieDuKassoulait/KassouBot/-/blob/main/docker-compose.yml) or in command line:

First you need to create a dedicated network for our containers:

On Debian-based distro:
```batch
docker network create -d bridge default-kassoubot 2> /dev/null || true
```
On Windows:
```batch
docker network create -d bridge default-kassoubot 2> NUL || REM
```

Then you can create our containers:
```batch
docker run -d \
    --name="Lavalink" \
    -p 2333:2333 \
    --network="default-kassoubot" \
    --restart=unless-stopped \
    registry.gitlab.com/confreriedukassoulait/lavalink:latest \
    lavalink \
    && \
docker run -d \
    --name="KassouBot" \
    -e TOKEN_DISCORD="yourTokenDiscord" \
    -e TOKEN_GENIUS="yourTokenGenius" \
    -e TOKEN_REDDIT_CLIENT_ID="yourRedditClientID" \
    -e TOKEN_REDDIT_CLIENT_SECRET="yourRedditClientSecret" \
    -e TOKEN_REDDIT_USER_AGENT="yourRedditUserAgent" \
    -e TIMEZONE="yourTimezone" \
    -e PREFIX="yourPrefix" \
    -v /here/your/path/:/opt/db \
    --network="default-kassoubot" \
    --restart=unless-stopped \
    registry.gitlab.com/confreriedukassoulait/kassoubot
```
- You shouldn't change at least the service name of the Lavalink container because the bot is using his DNS name (and it's based on the service's name).
- You can add the environment variable `DEACTIVATE` to disable some cogs (separate the cogs with commas and no spaces between).
- You can add the environment variable `REGION_DISCORD` to force the Lavalink server to use your voice region.

To find reddit tokens, go to [this site](https://www.reddit.com/prefs/apps) and here are the instructions: ![instructions](https://i.imgur.com/tEzYKDA.png)
*redirection uri (for copy/paste) : http://localhost:8080*

To find Genius token, go to [this site](https://genius.com/api-clients), `login to your account` and on the left select `New API Client`. Fill the field with what you want then click `Save`. Now your token is the `CLIENT ACCESS TOKEN`.

## __Add the bot to your server__

- In the [Discord Dev Portal](https://discord.com/developers/applications) create an application, and make sure it's a `Bot` (third tab).
- To invite it, go to the `OAuth2` (second tab) tab, select the scopes `bot` (required) and `applications.commands` (for the slashs commands) and in the bot permissions select `Administrator` (You can select manually at your own risk).
- You have the link to copy above between the two blocks `scopes` and `permissions`.
- If you need help, you can [join my Discord](https://discord.gg/Z5ePxH4).

## __Launching locally__
If you want to run it without Docker, clone the repo and his submodules by doing this command in the git folder:
```batch
git submodule update --force --recursive --init --remote
```
Then create an .env file to store variables in the root folder (there is an example [here](https://gitlab.com/ConfrerieDuKassoulait/KassouBot/-/blob/main/.envexample)).

Install all the requirements by doing `python3 -m pip install -r requirements.txt` (I recommend using virtualenv to not interfere with your system).

If you need to install Java, there is some step to have the same as the [Docker image built for the project](https://gitlab.com/ConfrerieDuKassoulait/lavalink/), on Debian-based distro:
```bash
wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
sudo add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
sudo apt update -y
sudo apt install adoptopenjdk-16-openj9 -y
```
On Windows just go [to their site](https://adoptopenjdk.net/?variant=openjdk16&jvmVariant=openj9).

Then, simply run `start.bat` if you are on Windows or `start.sh` if on Debian-based distro to launch the bot.

FROM python:3.9.5-slim

ENV DOCKER_KASSOUBOT=True

COPY requirements.txt .
RUN apt update && \
    apt install git curl -y && \
    pip install -r requirements.txt && \
    apt purge git -y

COPY README.md .

WORKDIR /opt
COPY src .

RUN curl -Lso lavalink/README.md https://gitlab.com/ConfrerieDuKassoulait/lavalink/-/raw/main/README.md && \
    apt purge curl -y

CMD ["python", "-u", "./main.py"]

@echo off
cd src\lavalink
if exist Lavalink.jar (
    echo "Lavalink found."
) else (
    echo "Downloading Lavalink ..."
    call download.bat
)
tasklist /FI "ImageName eq java.exe" 2> NUL | find /i "Console" > NUL
if %ERRORLEVEL% == 0 (
    echo "Lavaling already started."
) else (
    echo "Starting Lavalink server ..."
    start java -Djdk.tls.client.protocols=TLSv1.3 -Xmx3G -jar Lavalink.jar REM Starting the server in another window
)
cd ..
echo "Starting Discord bot ..."
python -u ./main.py
